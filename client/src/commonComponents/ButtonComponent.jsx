import React from "react";
import Button from "@material-ui/core/Button";

export const ButtonComponent = ({
  variant = "contained",
  type,
  color = "default",
  className,
  btnName,
  onClick,
}) => {
  return (
    <Button
      variant={variant}
      color={color}
      type={type}
      className={className}
      onClick={onClick}
    >
      {btnName ?? <span className="navbar-toggler-icon"></span>}
    </Button>
  );
};

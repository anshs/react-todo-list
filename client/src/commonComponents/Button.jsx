import React from "react";

export const Button = React.memo(
  ({ className, type, btnName, onClick, ...props }) => {
    return (
      <button
        className={className}
        type={type}
        data-bs-toggle="collapse"
        data-bs-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
        onClick={onClick}
        {...props}
      >
        {btnName ?? <span className="navbar-toggler-icon"></span>}
      </button>
    );
  }
);
Button.defaultProps = {
  className: "btn btn-primary",
  type: "button",
};

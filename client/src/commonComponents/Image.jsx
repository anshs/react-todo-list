import React from "react";

export const Image = React.memo(({ src, alt, height, width }) => {
  return <img src={src} alt={alt} height={height} width={width} />;
});
Image.defaultProps = {
  height: "100px",
  width: "100px",
};

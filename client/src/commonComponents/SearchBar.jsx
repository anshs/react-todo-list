import React from "react";
import { Button } from "./Button";

export const SearchBar = () => {
  return (
    <form className="d-flex me-2 pe-5" role="search">
      <input
        className="form-control me-2"
        type="search"
        placeholder="Search"
        aria-label="Search"
      />
      <Button className="btn btn-warning" type="submit" btnName="Search" />
    </form>
  );
};

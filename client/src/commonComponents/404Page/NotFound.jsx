import React from "react";
import { Link } from "react-router-dom";
import { GoHome } from "react-icons/go";
import "./NotFound.css";

export const NotFound = () => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <div className="error-template">
            <h1>Oops!</h1>
            <h2>404 Page Not Found</h2>
            <div className="error-details">
              Sorry, an error has occured, Requested page not found!
            </div>
            <div className="error-actions">
              <Link to="/" className="btn btn-primary">
                <GoHome className="home" /> Home{" "}
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

import React from "react";

export const Input = React.memo(
  ({ type, value, onChange, className, placeholder }) => {
    return (
      <input
        type={type}
        value={value}
        onChange={onChange}
        className={className}
        placeholder={placeholder}
      />
    );
  }
);

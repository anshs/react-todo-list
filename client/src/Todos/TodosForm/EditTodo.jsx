import React, { useContext, useState } from "react";
import { Input, ButtonComponent } from "../../commonComponents";
import { TodosContext } from "../../App";
import "../Todo.css";

export const EditTodo = React.memo(() => {
  const { editTodo, onCancelBtn, todoToBeEdited } = useContext(TodosContext);
  const [title, setTitle] = useState(todoToBeEdited?.title);
  const [desc, setDesc] = useState(todoToBeEdited?.description);

  const onSave = () => {
    if (!title || !desc) {
      alert("Title or Description cannot be empty!");
    } else {
      const updatedTodo = {
        id: todoToBeEdited.id,
        title: title,
        description: desc,
      };
      editTodo(updatedTodo);
      setTitle("");
      setDesc("");
    }
  };

  return (
    <>
      <form
        className="edit-todo-form"
        onSubmit={() => {
          const confirmBox = window.confirm(
            "Are you sure you want to update todo?"
          );
          if (confirmBox === true) {
            onSave();
          }
        }}
      >
        <h3 className="header edit-todo-header">Edit Todo</h3>
        <hr />
        <div className="mb-3">
          <label htmlFor="title" className="form-label labelStyle">
            Title :-
          </label>
          <Input
            type="text"
            className="form-control"
            value={title}
            placeholder="Enter your Title"
            onChange={(e) => setTitle(e.target.value)}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="description" className="form-label labelStyle">
            Description :-
          </label>
          <Input
            type="text"
            className="form-control"
            value={desc}
            placeholder="Enter your Description"
            onChange={(e) => setDesc(e.target.value)}
          />
        </div>
        <br />
        <div className="row">
          <div className="col-md-2">
            <ButtonComponent
              type="submit"
              btnName="Cancel"
              onClick={() => {
                onCancelBtn();
              }}
            />
          </div>
          <div className="col div-btn-save">
            <ButtonComponent type="submit" color="primary" btnName="Save" />
          </div>
        </div>
      </form>
    </>
  );
});

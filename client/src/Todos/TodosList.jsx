import React from "react";
import { TodoItem } from "./TodoItem";

export const TodosList = React.memo(({ todos }) => {
  const headerStyle = {
    color: "red",
    marginTop: "15px",
    marginBottom: "20px",
  };

  return (
    <div className="container ms-2 mt-2">
      {todos.length === 0 ? (
        <h3 style={headerStyle}>No Todos to display!</h3>
      ) : (
        <>
          <h3 style={headerStyle}>Todo List</h3>
          <br />
          <div className="row todo-item">
            <div className="col-md-3">
              <b>Title</b>
            </div>
            <div className="col-md-3">
              <b>Description</b>
            </div>
            <div className="col-md-3">
              <b>Actions</b>
            </div>
          </div>
          {todos.map((todo) => {
            return <TodoItem key={todo.id} {...todo} />;
          })}
        </>
      )}
    </div>
  );
});

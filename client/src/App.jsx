import React, { useState, useEffect, createContext, useCallback } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { Todos, About, Users, Person } from "./Pages";
import "./App.css";
import { NotFound } from "./commonComponents/404Page/NotFound";
import { Layout } from "./Layouts/Layout";

// create a context
const TodosContext = createContext();

function App() {
  const initTodo =
    localStorage.getItem("todos") === null
      ? []
      : JSON.parse(localStorage.getItem("todos"));

  // todos State
  const [todos, setTodos] = useState(initTodo);
  useEffect(() => {
    localStorage.setItem("todos", JSON.stringify(todos));
  }, [todos]);

  // Add new Todo
  const addTodo = useCallback(
    (title, desc) => {
      const newTodo = {
        id: Math.floor(Math.random() * 100000000),
        title,
        description: desc,
      };
      if (todos.length === 0) {
        setTodos([newTodo]);
      }
      console.log("new Todo :", newTodo);
      setTodos([...todos, newTodo]);

      localStorage.setItem("todos", JSON.stringify(todos));
    },
    [todos]
  );

  // Delete Todo
  const onDelete = (id) => {
    console.log("Deleting todo with Id: ", id);

    setTodos(
      todos.filter((todo) => {
        return todo.id !== id;
      })
    );
  };

  const [isEdit, setEditMode] = useState(false);
  const [todoToBeEdited, setTodoToBeEdited] = useState();

  const onEdit = useCallback(
    (id) => {
      setEditMode(true);
      const myTodo = todos.find((todo) => todo.id === parseInt(id));
      console.log("Going to edit : ", myTodo);

      setTodoToBeEdited(myTodo);
    },
    [todos]
  );

  // Edit Todo
  const editTodo = (updatedTodo) => {
    const updatedTodos = todos.filter((todo) => {
      if (todo.id === parseInt(updatedTodo.id)) {
        todo.id = updatedTodo.id;
        todo.title = updatedTodo.title;
        todo.description = updatedTodo.description;

        return todo;
      }

      return todo;
    });

    setTodos(updatedTodos);
  };

  const onCancelBtn = useCallback(() => {
    setEditMode(false);
  }, []);

  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <TodosContext.Provider
            value={{
              todos,
              isEdit,
              todoToBeEdited,
              editTodo,
              onCancelBtn,
              addTodo,
              onDelete,
              onEdit,
            }}
          >
            <Layout component={<Todos isEdit={isEdit} />} />
          </TodosContext.Provider>
        </Route>
        <Route exact path="/about" component={About}></Route>
        <Route exact path="/users">
          <Layout component={<Users />} />
        </Route>
        <Route
          path="/user/:id"
          children={<Layout component={<Person />} />}
        ></Route>
        <Route path="*" component={NotFound}></Route>
      </Switch>
    </Router>
  );
}

export default App;
export { TodosContext };

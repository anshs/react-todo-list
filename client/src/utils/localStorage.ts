export const getLocalStorageKey = (key: string) => {
  return localStorage.getItem(key);
};

export const setLocalStorageKey = (key: string, data: string) => {
  localStorage.setItem(key, data);
};

export const resetLocalStorage = () => {
  localStorage.clear();
};

import { getLocalStorageKey } from "./localStorage";
import { appConfig } from "../config";
import { LocalStorageVariables } from "../constants/enums";

interface PostProps {
  url: string;
  data?: any;
  addToken?: boolean;
}

const authToken = getLocalStorageKey(LocalStorageVariables.authToken);

export const getApi = async (url: string, addToken = true) => {
  const response = await fetch(`${appConfig.apiURL}/${url}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      ...(addToken && authToken && { Authorization: `Bearer ${authToken}` }),
    },
  });

  return response.json();
};

export const postApi = async ({ url, data, addToken = true }: PostProps) => {
  const response = await fetch(`${appConfig.apiURL}/${url}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      ...(addToken && authToken && { Authorization: `Bearer ${authToken}` }),
    },
    body: JSON.stringify(data),
  });

  return response.json();
};

export const patchApi = async ({ url, data, addToken = true }: PostProps) => {
  const response = await fetch(`${appConfig.apiURL}/${url}`, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
      ...(addToken && { Authorization: `Bearer ${authToken}` }),
    },
    body: JSON.stringify(data),
  });

  return response.json();
};

// export const showErrorToaster = (data: ErrorResponse) => {
//   if (data.statusCode === 401) {
//     window.location.href = routerPaths.login;
//     resetLocalStorage();
//   } else if (data.message) {
//     Toaster(ToasterType.error, data.message);
//   }
// };

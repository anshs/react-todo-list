import React from "react";
import Prototypes from "prop-types";
import { Link } from "react-router-dom";
import { SearchBar } from "../commonComponents";
import "./Layout.css";

export const Header = React.memo(({ title, searchBar }) => {
  return (
    <nav className="navbar navbar-expand-lg bg-info">
      <div className="container-fluid">
        <Link className="navbar-brand active ms-3" to="/">
          {title}
        </Link>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <Link className="nav-link ms-3" aria-current="page" to="/">
                Todos
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link ms-3" to="/about">
                About
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link ms-3" to="/users">
                Users
              </Link>
            </li>
          </ul>
          {searchBar ? <SearchBar /> : ""}
        </div>
      </div>
    </nav>
  );
});
Header.defaultProps = {
  title: "Today's timetable",
  searchBar: true,
};
Header.propType = {
  title: Prototypes.string,
  searchBar: Prototypes.bool,
};

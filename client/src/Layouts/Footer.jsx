import React from "react";
import "./Layout.css";

export const Footer = () => {
  const footerStyle = {
    position: "relative",
    padding: "20px 5px 5px 5px",
    width: "100%",
    top: "40vh",
  };
  return (
    <footer className="bg-dark text-light" style={footerStyle}>
      <p className="text-center">
        Copyrights&nbsp; &copy; &nbsp; 2022 &nbsp; myTodoList.com
      </p>
    </footer>
  );
};

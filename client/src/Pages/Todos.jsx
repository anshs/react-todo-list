import React, { useContext } from "react";
import { TodosContext } from "../App";
import { AddTodo, EditTodo, TodosList } from "../Todos";

export const Todos = React.memo(({ isEdit }) => {
  const { todos, addTodo } = useContext(TodosContext);
  return (
    <>
      <br />
      <br />
      <div className="row">
        <div className="col-md-6 col-sm-8">
          {isEdit ? <EditTodo /> : <AddTodo addTodo={addTodo} />}
        </div>
        <div className="col-md-6 col-sm-4">
          <TodosList todos={todos} />
        </div>
      </div>
    </>
  );
});

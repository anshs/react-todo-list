import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import { useFetch } from "../CustomHooks/useFetch";
import { appConfig } from "../../config";
import { Image, Loader } from "../../commonComponents";
import "./Person.css";

export const Person = () => {
  const { id } = useParams();
  const [name, setPersonName] = useState("");
  const { isLoading, users } = useFetch(appConfig.githubUsersApi);

  useEffect(() => {
    const person = users.find((user) => user.id === parseInt(id));
    setPersonName(person);
  }, [users, id]);
  return isLoading ? (
    <Loader />
  ) : (
    <div className="row">
      <div className="col-md-4"></div>
      <div className="col-md-4">
        <h2 className="user-header">Person details :-</h2>
        <hr />
        <div className="details">
          <Image
            src={name.avatar_url}
            alt={name.login}
            height="300px"
            width="300px"
          />
          <p>
            <a href={name.html_url}>Profile</a>
          </p>
          <p>
            Name : <b>{name.login}</b>
          </p>
        </div>
      </div>
      <div className="col-md-4"></div>
    </div>
  );
};

import { useCallback, useEffect, useState } from "react";
import axios from "axios";

export const useFetch = (url) => {
  const [users, setUsers] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const getUsers = useCallback(async () => {
    const response = await axios.get(url);

    setUsers(response.data);
    setTimeout(() => {
      setIsLoading(false);
    }, 1500);
  }, [url]);

  useEffect(() => {
    getUsers();
  }, [url, getUsers]);

  return { isLoading, users };
};

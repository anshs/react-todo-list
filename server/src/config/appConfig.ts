import { config } from 'dotenv';

config();

export const appConfig = {
  port: process.env.PORT,
  connectionUrl: process.env.MONGODB_CONNECTION_URL,
};

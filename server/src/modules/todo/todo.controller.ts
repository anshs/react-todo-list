import { Body, Controller, Get, Post } from '@nestjs/common';
import { TodoService } from './todo.service';

@Controller('todos')
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  @Get()
  async getAllTodos() {
    return 'done';
  }

  @Post()
  async createTodo(@Body() createTodoDto: any) {
    try {
      return this.todoService.createTodo(createTodoDto);
    } catch (error) {
      throw new Error(error.message);
    }
  }
}

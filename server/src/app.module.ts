import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { appConfig } from './config/appConfig';
import { TodoModule } from './modules/todo/todo.module';
import { UserModule } from './modules/user/user.module';

@Module({
  imports: [
    MongooseModule.forRoot(appConfig.connectionUrl),
    TodoModule,
    UserModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
